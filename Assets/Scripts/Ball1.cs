using UnityEngine;
using Mirror;

public class Ball1 : NetworkBehaviour
{
    //vitesse de la balle
    public float speed = 30;
    //rigidBody de la ball
    public Rigidbody2D rigidbody2d;

    public override void OnStartServer()
    {
        base.OnStartServer();

        //On simule la physic de la balle seulement coté server
        rigidbody2d.simulated = true;

        //Lance la ball vers le haut au debut de la partied
        rigidbody2d.velocity = Vector2.up * speed;
    }

    float HitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight)
    {
        //Physique du rebond de la balle
        return (ballPos.y - racketPos.y) / racketHeight;
    }


    // only call this on server
    [ServerCallback]
    public void Respawn()
    {
        transform.position = Vector3.zero;
        GetComponent<Rigidbody2D>().velocity = Random.insideUnitSphere.normalized * speed;
        Debug.Log(GetComponent<Rigidbody2D>().velocity);

    }

    //Appelle coté serveur
    [ServerCallback]
    void OnCollisionEnter2D(Collision2D col)
    {
        //si touche la raquette calcule la trajectoir de rebond
        if (col.transform.GetComponent<Player>())
        {
            //Calcule la direction y
            float y = HitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);

            //Calcule la direction x
            float x = col.relativeVelocity.x > 0 ? 1 : -1;

            //Applique la nouvelle direction x&y
            Vector2 dir = new Vector2(x, y).normalized;

            //applique direction multiplié par la vitesse
            rigidbody2d.velocity = dir * speed;
        }
    }
}

