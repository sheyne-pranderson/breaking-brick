using UnityEngine;
using Mirror;

public class Player : NetworkBehaviour
{
    //vitesse de déplacement du joueur
    public float speed = 1500;
    //rigidBody du joeur
    public Rigidbody2D rigidbody2d;

    //variable envoyé en P2P
    [SyncVar]
    //true si joueur 1
    public bool isPlayer1;
    //couleur du joueur1
    public Color p1color;
    //couleur du joueur2
    public Color p2color;


    void Start()
	{
        //change la couleur de la raquette en fonction de leur role (J1 ou J2)
        if (isPlayer1)
        {
            gameObject.GetComponent<SpriteRenderer>().color = p1color;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = p2color;
        }

    }

    void FixedUpdate()
    {
        // permet au joeur local de controller sa raquette (et non pas celle de l'adversaire)
        if (isLocalPlayer)
		{
            rigidbody2d.velocity = new Vector2(Input.GetAxisRaw("Horizontal"), 0) * speed * Time.fixedDeltaTime;
		}
           
    }
}
