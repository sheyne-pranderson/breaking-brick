using UnityEngine;
using Mirror;

public class ChangeColor : NetworkBehaviour
{
	//change la couleur de la raquette
    [Command(ignoreAuthority = true)]
	public void CmdChangeColor(GameObject player, Color playerColor)
	{
		player.GetComponent<SpriteRenderer>().color = playerColor;
	}
}
