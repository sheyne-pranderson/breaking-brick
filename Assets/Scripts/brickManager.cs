using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UIElements;

public class brickManager : NetworkBehaviour
{
    //nombre de ligne de brique
    public int rows;
    //nombre de colonne de brique
    public int collumns;
    //espace entre chaque prique
    public float spacing;
    //gameObject du networkmanager;
    public NetworkManagerBrick nm;

    //vide et reset le niveau
    public void ResetLevel(GameObject _brick)
	{
        //vide le niveau
        viderLvl();

        //genere le bon nopmbre de brique par ligne et par colonne
		for (int x = 0; x < collumns; x++)
		{
			for (int y = 0; y < rows; y++)
			{
                Vector2 spawnPos = (Vector2)transform.position + new Vector2(
                    x * (_brick.transform.localScale.x + spacing),
                    -y * (_brick.transform.localScale.y + spacing)
                    );
                GameObject brick = Instantiate(_brick, spawnPos, Quaternion.identity);

                //ajoute la brique
                NetworkServer.Spawn(brick);
			}
		}
	}

    //enleve la brique et lance le reset si c'etait la dernière brique
    public void RemoveBrick(brick brik)
	{
        NetworkServer.Destroy(brik.gameObject);
        //Debug.Log(GameObject.FindGameObjectsWithTag("brick").Length);
        if (GameObject.FindGameObjectsWithTag("brick").Length == 1)
		{
            //Debug.Log("reseting b");
            ResetLevel(nm.brick);
		}
	}

    //vide le niveau de toute les briques
    public void viderLvl()
	{
		//foreach (GameObject brick in bricks)
		//{
		//    //Destroy(brick);
		//    NetworkServer.Destroy(gameObject);
		//}
		//bricks.Clear();

		if (GameObject.FindGameObjectsWithTag("brick").Length !=0)
		{
			foreach (GameObject item in GameObject.FindGameObjectsWithTag("brick"))
			{
                NetworkServer.Destroy(item);
            }
		}
    }
}
