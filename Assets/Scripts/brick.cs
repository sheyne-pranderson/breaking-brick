using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class brick : NetworkBehaviour
{
    //public int points = 10;

    //appele quand subis une collision
    public void OnCollisionEnter2D(Collision2D collision)
    {
        //seulement si la colision a lieu du coté serveur alors elle est prise en compte
        if (collision.gameObject.GetComponent<NetworkIdentity>().isServer)
        {
            Bcollision();
        } 
    }

    //enleve la brick du brickManager
    public void Bcollision()
	{
        FindObjectOfType<brickManager>().RemoveBrick(this);
    }
}
