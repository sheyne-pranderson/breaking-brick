using UnityEngine;
using Mirror;

[AddComponentMenu("")]
public class NetworkManagerBrick : NetworkManager
{
	//position joueur 1
	public Transform player1;
	//position joueur 2
	public Transform player2;
	//gameObject de la ball
	GameObject ball;
	//gameobject de la brique
	public GameObject brick;

	//script du changement de couleur
	public ChangeColor scriptCC;

	//Gestion des briques
	public brickManager bm;

	//prefab de la balle
	public GameObject ballpref;

	//appele quand les joueurs sont connecté
	public override void OnServerAddPlayer(NetworkConnection conn)
	{
		//ajoute les joueurs à leur position de spawn par defaut
		Transform start = numPlayers == 0 ? player1 : player2;
		GameObject player = Instantiate(playerPrefab, start.position, start.rotation);

		//ici on défini qui est le joueur 1 (celui qui host) et le joueur 2
		if (numPlayers == 0)
		{
			player.GetComponent<Player>().isPlayer1 = true;
		}
		else
		{
			player.GetComponent<Player>().isPlayer1 = false;

		}

		NetworkServer.AddPlayerForConnection(conn, player);

		//la balle apparait quand les 2joueur sont connecté
		if (numPlayers == 2)
		{	
			//on reset le niveau
			bm.ResetLevel(brick);
			//on spawn la ball
			ball = Instantiate(spawnPrefabs.Find(prefab => prefab.name == ballpref.name));
			NetworkServer.Spawn(ball);

		}
	}

	//quand un/deux joueur est déconnecté
	public override void OnServerDisconnect(NetworkConnection conn)
	{
		//la ball est détruit
		if (ball != null)
			NetworkServer.Destroy(ball);

		//on vide les brique du niveau
		bm.viderLvl();

		//on déconnecte le joueur restant 
		base.OnServerDisconnect(conn);
	}


	public void debugColorPlayer()
	{
		//if (Input.GetKeyDown("space"))
		//{
		//	GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().color = p2color;

		//	print("space key was pressed");
		//}
	}

	public void debugLesBrick()
	{
		//if (Input.GetKeyDown("space"))
		//{
		//	if (GameObject.FindGameObjectWithTag("brick"))
		//	{
		//		GameObject.FindGameObjectWithTag("brick").GetComponent<brick>().Bcollision();
		//	}
		//	print("space key was pressed");
		//}
	}

	void Update()
	{
		//debugColorPlayer();
		//debugLesBrick();
	}
}